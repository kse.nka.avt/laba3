#include "server.h"

#include <QTcpServer>
#include <QTcpSocket>

#include <QTextEdit>
#include <QVBoxLayout>
#include <QLabel>
#include <QtSql>
#include <QSqlDatabase>
#include <QDataStream>
#include <QDateTime>
#include <openssl/evp.h>
#include <openssl/aes.h>
#include <QBuffer>
#include <QTextCodec>
#include "qaesencryption.h"

Server::Server(quint16 port): _nextBlockSize(0){
    _tcpServer = new QTcpServer(this);

    if (!_tcpServer->listen(QHostAddress::Any, port)){
        _tcpServer->close();
        return;
    }

    connect(_tcpServer, SIGNAL(newConnection()), this, SLOT(slotNewConnection()));

    _db = QSqlDatabase::addDatabase( "QSQLITE" );
    _db.setDatabaseName("/Users/ksenka/chat_laba3.sqlite");

    if (_db.open())
    {
        qDebug() << "Connnection established";
    }
    else
    {
        qDebug() << "Error = " << _db.lastError().text();
    }


    _text = new QTextEdit();
    _text->setReadOnly(true);
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(new QLabel("<H1>Server</H1>"));
    layout->addWidget(_text);
    setLayout(layout);
    resize(400,400);
}

void Server::slotNewConnection(){
    qDebug() << "New connection";
    sockets.append(_tcpServer->nextPendingConnection());
    connect(sockets.last(),SIGNAL(readyRead()),this, SLOT(slotReadClient()));
    connect(sockets.last(), &QTcpSocket::disconnected, this, &Server::ClientDisconnected);
}

void Server::ClientDisconnected()
{
    QTcpSocket* pClient = static_cast<QTcpSocket*>(QObject::sender());
    sockets.removeOne(pClient);
    qDebug() << sockets.size();
}

void Server::slotReadClient(){
    for (int i = 0; i < sockets.size(); i++)
    {
        QDataStream in(sockets.at(i));
        while(true)
        {
            if (_nextBlockSize == 0)
            {
                if (sockets.at(i)->bytesAvailable() <static_cast<int>(sizeof(quint16)))
                {
                    break;
                }
                in >> _nextBlockSize;
            }

            if (sockets.at(i)->bytesAvailable() < _nextBlockSize){
                break;
            }
            QByteArray type;
            in >> type;
            if (QString(type) == "<<<signup>>>"){
                QByteArray login_enc;
                QByteArray password_sha;
                in >> login_enc >> password_sha;
                QAESEncryption encryption(QAESEncryption::AES_128, QAESEncryption::ECB);
                QString login_dec = QString::fromUtf8(encryption.decode(login_enc, "0123456789abcdeF"));
                QSqlQuery query;
                QString str;
                QString strF = "INSERT INTO users (login, password) VALUES('%1', '%2');";
                str = strF.arg(login_dec.left(login_dec.length() - 1)).arg(QString(password_sha));
                if (!query.exec(str))
                {
                   qDebug() << _db.lastError().text();
                   _nextBlockSize = 0;
                   sendToClient(sockets.at(i), "err::signup::Такой логин уже есть :( Попробуй выбрать другой.");
                } else {
                    _nextBlockSize = 0;
                    sendToClient(sockets.at(i), "suc::signup::Отлично, а теперь нажми кнопку 'Войти'!");
                }
            } else if (QString(type) == "<<<auth>>>"){
                QByteArray login_enc;
                QByteArray password_sha;
                in >> login_enc >> password_sha;
                QAESEncryption encryption(QAESEncryption::AES_128, QAESEncryption::ECB);
                QString login_dec = QString::fromUtf8(encryption.decode(login_enc, "0123456789abcdeF"));
                QString message = "Client has sent - " + login_dec + " " + password_sha + + " .";
                _text->append(message);
                _nextBlockSize = 0;
                QSqlQuery query;
                QString str;
                QString strF = "SELECT login FROM users WHERE login='%1' AND password='%2';";
                str = strF.arg(login_dec.left(login_dec.length() - 1)).arg(QString(password_sha));

                if (!query.exec(str))
                {
                    _nextBlockSize = 0;
                    sendToClient(sockets.at(i), "err::"+query.lastError().text());
                } else {
                    if (query.next()){
                        _nextBlockSize = 0;
                        sendToClient(sockets.at(i), "suc::auth::Успех!");
                    } else {
                        _nextBlockSize = 0;
                        sendToClient(sockets.at(i), "err::auth::Неверный логин или пароль.");
                    }
                }
            } else if (QString(type) == "<<<message>>>"){
                QByteArray message_enc;
                QByteArray login_enc;
                QDateTime time;
                in >> message_enc >> login_enc >> time;
                QAESEncryption encryption(QAESEncryption::AES_128, QAESEncryption::ECB);
                QString message_dec = QString::fromUtf8(encryption.decode(message_enc, "0123456789abcdeF"));
                QString login_dec = QString::fromUtf8(encryption.decode(login_enc, "0123456789abcdeF"));
                QSqlQuery query;
                QString str;
                QString strF = "INSERT INTO chat (login, text, date) VALUES('%1', '%2', '%3');";
                str = strF.arg(login_dec.left(login_dec.length() - 1)).arg(message_dec.left(message_dec.length() - 1)).arg(time.toString("dd.MM.yyyy HH:mm:ss"));
                if (!query.exec(str))
                {
                    _nextBlockSize = 0;
                    sendToClient(sockets.at(i), "err::mess::"+query.lastError().text());
                } else {
                    for (int j = 0; j < sockets.size(); j++){
                        _nextBlockSize = 0;
                        sendToClient(sockets.at(j), "suc::mess::sendall");
                    }
                }
            } else if (QString(type) == "<<<get_messages>>>"){
                QSqlQuery query;
                QString str = "SELECT * FROM chat ORDER BY id DESC";
                if (!query.exec(str))
                {
                    _nextBlockSize = 0;
                    sendToClient(sockets.at(i), "err::get::"+query.lastError().text());
                } else {
                    QString result;
                    while (query.next()){
                        result += query.value(3).toString() + "(*)(*)(*)" + query.value(2).toString() + "(*)(*)(*)" + query.value(1).toString() + "////";
                    }
                    QAESEncryption encryption(QAESEncryption::AES_128, QAESEncryption::ECB);
                    QByteArray result_enc = encryption.encode(result.toLatin1(), "0123456789abcdeF");
                    _nextBlockSize = 0;
                    QByteArray arrBlock;
                    QDataStream out(&arrBlock, QIODevice::WriteOnly);
                    //out.setVersion(QDataStream::Qt_5_10);

                    out << quint16(0) << "suc" << "get" << result_enc;
                    out.device()->seek(0);
                    out << quint16(arrBlock.size() - static_cast<int>(sizeof(quint16)));

                    sockets.at(i)->write(arrBlock);
                }
            } else if (QString(type) == "<<<find>>>"){
                QByteArray to_find_enc;
                QByteArray category_enc;
                in >> to_find_enc >> category_enc;
                QAESEncryption encryption(QAESEncryption::AES_128, QAESEncryption::ECB);
                QString to_find_dec = QString::fromUtf8(encryption.decode(to_find_enc, "0123456789abcdeF"));
                QString category_dec = QString::fromUtf8(encryption.decode(category_enc, "0123456789abcdeF"));
                QSqlQuery query;
                QString str = "SELECT * FROM chat WHERE " + category_dec.left(category_dec.length() - 1) + " LIKE '%" + to_find_dec.left(to_find_dec.length() - 1) + "%' ORDER BY id DESC";
                if (!query.exec(str))
                {
                    _nextBlockSize = 0;
                    sendToClient(sockets.at(i), "err::get::"+query.lastError().text());
                } else {
                    QString result;
                    while (query.next()){
                        result += query.value(3).toString() + "(*)(*)(*)" + query.value(2).toString() + "(*)(*)(*)" + query.value(1).toString() + "////";
                    }
                    QAESEncryption encryption(QAESEncryption::AES_128, QAESEncryption::ECB);
                    QByteArray result_enc = encryption.encode(result.toLatin1(), "0123456789abcdeF");
                    _nextBlockSize = 0;
                    QByteArray arrBlock;
                    QDataStream out(&arrBlock, QIODevice::WriteOnly);
                    //out.setVersion(QDataStream::Qt_5_10);

                    out << quint16(0) << "suc" << "get" << result_enc;
                    out.device()->seek(0);
                    out << quint16(arrBlock.size() - static_cast<int>(sizeof(quint16)));

                    sockets.at(i)->write(arrBlock);
                }
            }
        }
    }
}

void Server::sendToClient(QTcpSocket* socket, const QString &str){


    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    //out.setVersion(QDataStream::Qt_5_10);

    out << quint16(0) << str.split("::")[0].toLatin1() << str.split("::")[1].toLatin1() << str.split("::")[2];
    out.device()->seek(0);
    out << quint16(arrBlock.size() - static_cast<int>(sizeof(quint16)));

    socket->write(arrBlock);
}

QByteArray Server::sha256(const QByteArray& text)
{
    unsigned int outLen = 0;
    QByteArray dataBuff;
    dataBuff.resize(EVP_MAX_MD_SIZE);
#if OPENSSL_VERSION_NUMBER < 0x10100000L
    EVP_MD_CTX* evpMdCtx = EVP_MD_CTX_create();
#else
    EVP_MD_CTX* evpMdCtx = EVP_MD_CTX_new();
#endif
    EVP_DigestInit(evpMdCtx, EVP_sha256());
    EVP_DigestUpdate(evpMdCtx, text.data(), static_cast<size_t>(text.size()));
    EVP_DigestFinal_ex(evpMdCtx, reinterpret_cast<unsigned char*>(dataBuff.data()), &outLen);
#if OPENSSL_VERSION_NUMBER < 0x10100000L
    EVP_MD_CTX_cleanup(evpMdCtx);
    OPENSSL_free(evpMdCtx);
#else
    EVP_MD_CTX_free(evpMdCtx);
#endif
    dataBuff.resize(static_cast<int>(outLen));
    return dataBuff.toHex();
}


