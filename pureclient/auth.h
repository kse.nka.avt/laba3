#ifndef AUTH_H
#define AUTH_H

#include <QWidget>
#include <user.h>
#include <QVBoxLayout>
#include <QComboBox>

namespace Ui {
class Auth;
}

class Auth : public QWidget
{
    Q_OBJECT

public:
    explicit Auth(user *user = nullptr);
    ~Auth();

private slots:
    void on_auth_clicked();
    void change_ui();
    void on_sign_clicked();
    void on_send_clicked();
    void on_find_clicked();
    void table_update(QTableWidget *table);
private:
    Ui::Auth *ui;
    user *_user;
    QLineEdit *line;
    QLineEdit *line_1;
    QComboBox *choice;
    QTableWidget *_table;
    QVBoxLayout *layout_1;
};

#endif // AUTH_H
