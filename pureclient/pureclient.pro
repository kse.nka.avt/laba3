#-------------------------------------------------
#
# Project created by QtCreator 2019-05-25T12:59:14
#
#-------------------------------------------------

QT       += core gui
QT       += network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pureclient
TEMPLATE = app

INCLUDEPATH += /usr/local/Cellar/openssl/1.0.2r/include
LIBS += -L/usr/local/Cellar/openssl/1.0.2r
LIBS += -L/usr/local/Cellar/openssl/1.0.2r/lib -lssl
LIBS += -L/usr/local/Cellar/openssl/1.0.2r/lib -lcrypto

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    client.cpp \
    test.cpp \
    user.cpp \
    auth.cpp \
    qaesencryption.cpp \
    chat.cpp \
    the_chat.cpp

HEADERS += \
        mainwindow.h \
    client.h \
    test.h \
    user.h \
    auth.h \
    qaesencryption.h \
    chat.h \
    the_chat.h

FORMS += \
        mainwindow.ui \
    auth.ui \
    test.ui \
    auth.ui \
    chat.ui \
    the_chat.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
