#include "mainwindow.h"
#include <QApplication>
#include "user.h"
#include "auth.h"
#include "client.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    user user("localhost", 8080);
    Auth auth(&user);
    auth.show();
    return a.exec();
}
