#include "auth.h"
#include "ui_auth.h"
#include "QComboBox"
#include <QMessageBox>

Auth::Auth(user *user) :
    ui(new Ui::Auth),
    _user(user)
{
    ui->setupUi(this);
    _user->_cur_win = this;
    this->setAttribute( Qt::WA_DeleteOnClose, false );
}

Auth::~Auth()
{
    delete ui;
}

void Auth::on_send_clicked()
{
    QString text = line->text();
    _user->slotSendToServer("<<<message>>>::" + text);
    line->setText("");
}

void Auth::change_ui(){
    ui->password->close();
    ui->login->close();
    ui->label->close();
    ui->label_2->close();
    ui->sign->close();
    ui->auth->close();
    layout_1 = new QVBoxLayout();
    QHBoxLayout *layout_2 = new QHBoxLayout();
    QHBoxLayout *layout_3 = new QHBoxLayout();
    line = new QLineEdit();
    line->setPlaceholderText("Введите сообщение");
    QPushButton *button_1 = new QPushButton();
    QPushButton *button_2 = new QPushButton();
    connect(button_1, SIGNAL(clicked()), this, SLOT(on_send_clicked()));
    connect(button_2, SIGNAL(clicked()), this, SLOT(on_find_clicked()));
    button_1->setText("Отправить");
    button_2->setText("Найти");
    QLabel *label = new QLabel();
    QLabel *label_1 = new QLabel();
    label_1->setText("<H4>Поиск:</H4>");
    _table = new QTableWidget();
    _table->setRowCount(1);
    line_1 = new QLineEdit();
    choice = new QComboBox();
    choice->addItem("login");
    choice->addItem("text");
    layout_3->addWidget(label_1);
    layout_3->addWidget(choice);
    layout_3->addWidget(line_1);
    layout_3->addWidget(button_2);
    label->setText("<H1>Чат</H1>");
    layout_1 ->addWidget(label);
    layout_2 ->addWidget(line);
    layout_2 ->addWidget(button_1);
    layout_1 ->addLayout(layout_2);
    layout_1 ->addLayout(layout_3);
    layout_1 ->addWidget(_table);
    setLayout(layout_1);
    resize(800,600);
    _user->slotSendToServer("<<<get_messages>>> ");
    this->setWindowTitle("Чат");
}

void Auth::on_auth_clicked()
{
    QString login = ui->login->text();
    QString password = ui->password->text();
    _user->_login = login;
    _user->slotSendToServer("<<<auth>>> " + login + " " + password);
}

void Auth::on_sign_clicked()
{
    QString login = ui->login->text();
    QString password = ui->password->text();

    if (password == "" || login == ""){
        QMessageBox::warning(this, "Внимание", "Необходимо ввести логин и пароль!");
    } else {
        _user->_login = login;
        _user->slotSendToServer("<<<signup>>> " + login + " " + password);
    }

}

void Auth::table_update(QTableWidget *table){
    layout_1->removeWidget(_table);
    _table = table;
    layout_1->addWidget(_table);
}

void Auth::on_find_clicked(){
    QString to_find = line_1->text();
    QString category = choice->currentText();
    _user->slotSendToServer("<<<find>>> " + to_find + " " + category);
}
