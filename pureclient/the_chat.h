#ifndef THE_CHAT_H
#define THE_CHAT_H

#include <QDialog>
#include "user.h"

namespace Ui {
class the_chat;
}

class the_chat : public QDialog
{
    Q_OBJECT

public:
    explicit the_chat(QWidget *parent = nullptr, user *user = nullptr);
    ~the_chat();

private slots:
    void on_send_clicked();

public slots:
    void table_update(QTableWidget *table);
private:
    Ui::the_chat *ui;
    user *_user;
};

#endif // THE_CHAT_H
