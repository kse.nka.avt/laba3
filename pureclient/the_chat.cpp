#include "the_chat.h"
#include "ui_the_chat.h"

the_chat::the_chat(QWidget *parent, user *user) :
    QDialog(parent),
    _user(user),
    ui(new Ui::the_chat)
{
    ui->setupUi(this);
    _user->slotSendToServer("<<<get_messages>>> ");
}

the_chat::~the_chat()
{
    delete ui;
}

void the_chat::on_send_clicked()
{
    QString text = ui->text->text();
    _user->slotSendToServer("<<<message>>>::" + text);
}

void the_chat::table_update(QTableWidget *table){
    qDebug() << "got table";
    ui->messages = table;
}
